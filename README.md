We will be assigning a single ticket to an issue and each branch will address one of these tickets. Before moving on to the next issue we will finish the previous branch and merge it to master.
Also we plan on making sure things like levels are managed and worked on by one person at a time by distributing the work in such a way that allows that so we can prevent merge conflicts on the files that cannot be diff by git.

 
